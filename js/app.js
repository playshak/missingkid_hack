var Notifier = {

  updatePreference: function(data) {
    var t = this;
    $.ajax({
      type: 'POST',
      url: "/",
      data: JSON.stringify(data),
      dataType: "json",
      complete: $.proxy(t.handleResponse, t)
    });
  },

  continueProcessingAnimation: false,
  startProcessingAnimation: function() {
    $("#subscribe-but").addClass('hide');
    $("#unsubscribe-but").addClass('hide');
    $("#process").removeClass('hide');
    this.continueProcessingAnimation = true;
    this.animateProcess();
  },

  stopProcessingAnimation: function() {
    this.continueProcessingAnimation = false;
    $("#process").addClass('hide');
  },

  currentOpacity: 1,
  animateProcess: function() {

    if (!this.continueProcessingAnimation) {
      return;
    }

    var t = this,
      newOpacity = 1;

    if (t.currentOpacity === 1) {
      newOpacity = 0.3;
    }
    t.currentOpacity = newOpacity;

    $("#process").animate({
      opacity: newOpacity
    }, {
      duration: 2000,
      complete: $.proxy(t.animateProcess, t)
    });
  },

  unsubscribe: function() {
    this.startProcessingAnimation();
    this.updatePreference({
      subscribe: false
    });
  },

  subscribe: function() {
    this.startProcessingAnimation();
    this.updatePreference({
      subscribe: true
    });
  },

  handleResponse: function(resp) {
    var r = JSON.parse(resp.responseText);
    this.stopProcessingAnimation();
    if (r.subscribe) {

      // show unsubscribe stuff
      $("#unsubscribe-but").removeClass('hide');
      $("#in_header").removeClass("hide");
      $("#header").addClass("hide");

      // hide subscribe stuff
      $("#subscribe-but").addClass('hide');
      $("#unsub-info").addClass('hide');
      $("#sub-info").removeClass('hide');

    } else {
      $("#in_header").addClass("hide");
      $("#header").removeClass("hide");

      $("#subscribe-but").removeClass('hide');
      $("#unsubscribe-but").addClass('hide');

      $("#sub-info").addClass('hide');
      $("#unsub-info").removeClass('hide');
    }

  },

  init: function() {
    var t = this,
      data = {
        subscribe: true
      };

    $("#subscribe-but").click(
      $.proxy(t.subscribe, t)
    );

    $("#unsubscribe-but").click(
      $.proxy(t.unsubscribe, t)
    );

  }
};

$(document).ready(
  $.proxy(Notifier.init, Notifier)
);