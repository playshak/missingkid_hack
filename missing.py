import os
import jinja2
import webapp2
import json
import urlparse

from google.appengine.ext import ndb
from google.appengine.api import urlfetch

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape', 'jinja2.ext.loopcontrols'])

class Missingkid(ndb.Model):
	"""Model an indiduval Guestbook with author, content, and date."""
	casenum = ndb.IntegerProperty()
	watcher_id = ndb.UserProperty()
	name = ndb.StringProperty()
	missingDate = ndb.DateTimeProperty()
	missingReason = ndb.StringProperty(indexed=False)
	age = ndb.IntegerProperty(indexed=False)
	missingLocation = ndb.StringProperty(indexed=False)
	contact = ndb.StringProperty(indexed=False)
	url = ndb.StringProperty(indexed=False)
	image = ndb.StringProperty(indexed=False)
	content = ndb.StringProperty(indexed=False)
	date = ndb.DateTimeProperty(auto_now_add=True)

class MainPage(webapp2.RequestHandler):

  def get(self):
      template = JINJA_ENVIRONMENT.get_template('templates/index.html')          
      self.response.write(template.render({'kids': Missingkid.query()}))

class TechPage(webapp2.RequestHandler):

  def get(self):
      template = JINJA_ENVIRONMENT.get_template('templates/tech.html')          
      self.response.write(template.render({}))

class KidPage(webapp2.RequestHandler):

  def get(self):
      id = self.request.get('id')
      kid = Missingkid.get_by_id(int(id))
      
      FLICKR_URL = """http://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=9bc0133ed3727f08dab3b9f2e4d30eee&user_id=83047641%40N08&format=json&nojsoncallback=1"""
      result = urlfetch.fetch(FLICKR_URL)
      fpics  = []
      if kid.casenum == 1220720 and result.status_code == 200:
        response = json.loads(result.content)
        photos = response['photos']['photo']
        if photos:
          i = 0
          for photo in photos:
            photoid = photo['id']
            farm = photo['farm']
            server = photo['server']
            secret = photo['secret']
            if (i < 2):
              i = i + 1;
              fpics.append({
                'url': 'http://farm' + str(farm) + '.staticflickr.com/' + str(server) + '/' + str(photoid) + '_' + secret + '.jpg',
                'id': photoid
                })

      template = JINJA_ENVIRONMENT.get_template('templates/kid.html')          
      self.response.write(template.render({'kid': kid, 'fpics': fpics, 'fpics_len': len(fpics)}))

class Fetch(webapp2.RequestHandler):
	def get(self):
		YQL_URL = """http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20xml%20where%20url%20=%20'http://www.missingkids.com/missingkids/servlet/XmlServlet?act=rss';&format=json&diagnostics=true"""
		result = urlfetch.fetch(YQL_URL)
		if result.status_code == 200:
			response = json.loads(result.content)
			items = response['query']['results']['rss']['channel']['item'];
			if items:
				for item in items:
					reasonname = item['title'].split(':')
					name = reasonname[1]
					url = item['link']
					caseNum = urlparse.parse_qs(url)['caseNum'][0]
					desc = item['description']
					imaget = item['enclosure']['url']
					image = imaget.replace('t.jpg','.jpg')
					kid = Missingkid()
					kid.casenum = int(caseNum)
					kid.name = name
					kid.url = url
					kid.image = image
					kid.content = desc
					kid.put()
					
class Flickr(webapp2.RequestHandler):	
	def get(self):
		FLICKR_URL = """http://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=9bc0133ed3727f08dab3b9f2e4d30eee&user_id=83047641%40N08&format=json&nojsoncallback=1"""
		result = urlfetch.fetch(FLICKR_URL)
		pics = []
		if result.status_code == 200:
			response = json.loads(result.content)
			photos = response['photos']['photo']
			if photos:
				for photo in photos:
					photoid = photo['id']
					farm = photo['farm']
					server = photo['server']
					secret = photo['secret']
					pics.append('http://farm' + str(farm) + '.staticflickr.com/' + str(server) + '/' + str(photoid) + '_' + secret + '.jpg')
		for pic in pics:
			self.response.write(pic)
			self.response.write('<br/>')



application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/fetch', Fetch),
    ('/flickr', Flickr),
    ('/kid', KidPage),
    ('/tech', TechPage)
  ], debug=True)


